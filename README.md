## Démonstrateur docker.isima.fr

Quelque soit votre projet nous vous recommandons d'utiliser la commande `docker compose`, plutôt que la commanbde `docker`.

Soit un `docker-compose.yml` qui permet de construire une image docker à partir de l'image `hello-world` de [https://hub.docker.com ](https://hub.docker.com) ayant pour nom `docker.isima.fr/vimazeno/helloworld` à partir du fichier `Dockerfile` accessible via le dossier courant `.`

```
services:
  hello_world:
    image: docker.isima.fr/vimazeno/helloworld
    container_name: docker.isima.fr/vimazeno
    build:
      context: .
      dockerfile: Dockerfile
```

* construire l'image 

    ```
    docker compose build
    ```

* lister l'image créée

    ```
    docker image ls 
    ```
    affiche

    ```
    docker.isima.fr/vimazeno/helloworld                                                                    latest             8c9e56bebe78   13 months ago   13.3kB
    ```

* s'authentifier sur docker.isima.fr
    ```
    docker login docker.isima.fr -u vimazeno
    ```
* pousser l'image que l'on vient de construire sur docker.isima.fr en la taggant latest
    ```
    docker push docker.isima.fr/vimazeno/helloworld:latest
    ```
* tagger l'image que l'on vient de construire avec le hash du commit git courant
    ```
    docker tag docker.isima.fr/vimazeno/helloworld:latest docker.isima.fr/vimazeno/helloworld:$(git rev-parse --short HEAD)
    ```
* pousser l'image que l'on vient de construire sur docker.isima.fr taggée avec le hash du commit git courant
    ```
    docker push docker.isima.fr/vimazeno/helloworld:$(git rev-parse --short HEAD)
    ```
* utilisation du projet par un utilisateur lambda (pas d'authentification nécessaire)
    ```
    git clone https://gitlab.isima.fr/vimazeno/helloworld.git
    docker-compose up
    ```
    l'image est automatiquement téléchargée à partir de docker.isima.fr